# texlab

A cross-platform implementation of the Language Server Protocol providing rich
cross-editing support for the LaTeX typesetting system.

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## NOTE

- Currently, there's a workaround in place for latexindent. It is **always**
  called with the `-l` CLI option. This fixes detection of latexindent configs
  when used with nvim LSP.


## What is texlab?

A cross-platform implementation of the Language Server Protocol providing rich
cross-editing support for the LaTeX typesetting system.

See [the project website](https://github.com/latex-lsp/texlab)


## How to use this image

This image is meant to be a one-to-one replacement of a natively compiled and
installed `texlab`. To compile a document in your current working directory,
call it like this:

```bash
$ podman run --rm -it -v $PWD:$PWD:z -w $PWD registry.gitlab.com/c8160/lsp/texlab [ARGS ...]
```

Or, if you want a more convenient wrapper, have a look [here][1]

## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/lsp/texlab


[1]: https://gitlab.com/c8160/shell-helpers/-/blob/main/texlab
