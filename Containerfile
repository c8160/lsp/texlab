FROM docker.io/rust:alpine AS BUILDER

RUN apk add --no-cache musl-dev
RUN cargo install texlab

FROM registry.gitlab.com/c8160/latex/tectonic:latest

ENV NAME=texlab ARCH=x86_64
LABEL   name="$NAME" \
        architecture="$ARCH" \
        run="podman run IMAGE ..." \
        summary="LSP for LaTeX" \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/lsp/texlab"

RUN microdnf install -y texlive-latexindent texlive-chktex; \
    rm -rf /var/cache

COPY --from=BUILDER /usr/local/cargo/bin/texlab /usr/bin/texlab
COPY help.md /
COPY hacks/latexindent /usr/local/bin/

ENTRYPOINT [ "/usr/bin/texlab" ]
